package com.stpi.BlockView.lib;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;

public abstract class GridItem {
	public String title = "item";
	public Drawable drawable;
	Class<Activity> intent;
	View.OnClickListener mListener;

	public GridLayout.LayoutParams layoutParams;
	int color = Color.WHITE;
	int textColor = Color.BLACK;

	public GridItem() {
		layoutParams = new GridLayout.LayoutParams();
		layoutParams.setMargins(5, 5, 5, 5);
		layoutParams.setGravity(Gravity.FILL);
		layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED);
		layoutParams.rowSpec = GridLayout.spec(GridLayout.UNDEFINED);
		layoutParams.setGravity(Gravity.CENTER);
	}

	public void setOnClickListener(View.OnClickListener listener) {
		this.mListener = listener;
	}
}
