package com.stpi.BlockView.lib;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;

/**
 * A ScrollLayout that can scroll to horizontal or vertical direction.
 * 
 */
public class ScrollLayout extends ViewGroup {
    private static final String TAG = "ScrollLayout";
    public static int SCROLL_HORIZONTAL = 1;
    public static int SCROLL_VERTICAL = 2;
    private int mScrollDirection = ScrollLayout.SCROLL_HORIZONTAL;
    
    private Scroller mScroller;
    public static boolean startTouch = true;
    
    /*
     * A speed ​​tracker, mainly in order to determine whether fling through the
     * slide sliding.
     */
    private VelocityTracker mVelocityTracker;
    
    /*
     * Record current right-most screen position. the value is limited between 0
     * and getRightMostScreen().
     */
    private static int mCurScreenX;
    
    /*
     * Record current bottom-most screen position. the value is limited between
     * 0 and getBottomMostScreen()
     */
    private static int mCurScreenY;
    
    /*
     * Touch states: 0：rest 1：scrolling
     */
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    
    /*
     * record touch state.
     */
    private int mTouchState = ScrollLayout.TOUCH_STATE_REST;
    private static final int SNAP_VELOCITY = 600;
    
    /*
     * the scroll distance that is gotten from touch event.
     */
    private int mTouchSlop;
    
    /*
     * The previous touch position.
     */
    private float mLastMotionX;
    private float mLastMotionY;
    
    private OnScrollToScreenListener onScrollToScreen = null;
    
    public ScrollLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public ScrollLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        this.mScroller = new Scroller(context);
        this.mTouchSlop = ViewConfiguration.get(this.getContext())
                .getScaledTouchSlop();
    }
    
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft = 0;
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View childView = this.getChildAt(i);
            if (childView.getVisibility() != View.GONE) {
                final int childWidth = childView.getMeasuredWidth();
                childView.layout(childLeft, 0, childLeft + childWidth,
                        childView.getMeasuredHeight());
                childLeft += childWidth;
            }
        }
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.e(ScrollLayout.TAG, "onMeasure");
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int width = MeasureSpec.getSize(widthMeasureSpec);
        final int height = MeasureSpec.getSize(heightMeasureSpec);
        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        if (widthMode != MeasureSpec.EXACTLY) {
            throw new IllegalStateException(
                    "ScrollLayout only canmCurScreen run at EXACTLY mode!");
        }
        
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (heightMode != MeasureSpec.EXACTLY) {
            throw new IllegalStateException(
                    "ScrollLayout only can run at EXACTLY mode!");
        }
        
        // The children are given the same width and height as the scrollLayout
        final int count = this.getChildCount();
        for (int i = 0; i < count; i++) {
            this.getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
        }
        this.scrollTo(ScrollLayout.mCurScreenX * width,
                ScrollLayout.mCurScreenY * height);
        this.doScrollAction(ScrollLayout.mCurScreenX, ScrollLayout.mCurScreenY);
    }
    
    /**
     * scroll to destination through current position.
     */
    public void snapToDestination() {
        final int screenWidth = this.getWidth();
        final int screenHeight = this.getHeight();
        final int destScreenX = (this.getVirtualScrollX() + screenWidth / 2)
                / screenWidth;
        final int destScreenY = (this.getVirtualScrollY() + screenHeight / 2)
                / screenHeight;
        this.snapToScreen(destScreenX, destScreenY);
    }
    
    /**
     * 
     * scroll to whichScreen position(the first position is 0).
     * 
     * @param whichScreenX
     * @param whichScreenY
     */
    public void snapToScreen(int whichScreenX, int whichScreenY) {
        // get the valid layout page
        whichScreenX = this.calcScreenX(whichScreenX);
        whichScreenY = this.calcScreenY(whichScreenY);
        if (this.getVirtualScrollX() != (whichScreenX * this.getWidth())
                || this.getVirtualScrollY() != (whichScreenX * this.getHeight())) {
            final int deltaX = whichScreenX * this.getWidth()
                    - this.getVirtualScrollX();
            final int deltaY = whichScreenY * this.getHeight()
                    - this.getVirtualScrollY();
            this.mScroller.startScroll(this.getVirtualScrollX(), this
                    .getVirtualScrollY(), deltaX, deltaY,
                    (Math.abs(deltaX) + Math.abs(deltaY)) * 2);
            ScrollLayout.mCurScreenX = whichScreenX;
            ScrollLayout.mCurScreenY = whichScreenY;
            this.doScrollAction(ScrollLayout.mCurScreenX,
                    ScrollLayout.mCurScreenY);
            this.invalidate(); // Redraw the layout
        }
    }
    
    /**
     * Jump to a screen position.
     * 
     * @param whichScreenX
     * @param whichScreenY
     */
    public void setToScreen(int whichScreenX, int whichScreenY) {
        whichScreenX = this.calcScreenX(whichScreenX);
        whichScreenY = this.calcScreenY(whichScreenY);
        ScrollLayout.mCurScreenX = whichScreenX;
        ScrollLayout.mCurScreenY = whichScreenY;
        this.scrollTo(whichScreenX * this.getWidth(), whichScreenY
                * this.getHeight());
        this.doScrollAction(whichScreenX, whichScreenY);
    }
    
    private int calcScreenX(int whichScreenX) {
        return Math.max(0, Math.min(whichScreenX, this.getRightMostScreen()));
    }
    
    private int calcScreenY(int whichScreenY) {
        return Math.max(0, Math.min(whichScreenY, this.getBottomMostScreen()));
    }
    
    public int getCurScreen() {
        return ScrollLayout.mCurScreenX;
    }
    
    @Override
    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            this.scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            this.postInvalidate();
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        
        this.mVelocityTracker.addMovement(event);
        final int action = event.getAction();
        final float x = (this.mScrollDirection & ScrollLayout.SCROLL_HORIZONTAL) == 0 ? 0
                : event.getX();
        final float y = (this.mScrollDirection & ScrollLayout.SCROLL_VERTICAL) == 0 ? 0
                : event.getY();
        switch (action) {
        case MotionEvent.ACTION_DOWN:
            Log.d(ScrollLayout.TAG, "event down!");
            
            if (!this.mScroller.isFinished()) {
                this.mScroller.abortAnimation();
            }
            this.mLastMotionX = x;
            this.mLastMotionY = y;
            break;
        
        case MotionEvent.ACTION_MOVE:
            int deltaX = (int) (this.mLastMotionX - x);
            int deltaY = (int) (this.mLastMotionY - y);
            this.mLastMotionX = x;
            this.mLastMotionY = y;
            this.scrollBy(deltaX, deltaY);
            break;
        
        case MotionEvent.ACTION_UP:
            Log.d(ScrollLayout.TAG, "event up!");
            final int screenWidth = this.getWidth();
            final int screenHeight = this.getHeight();
            int destScreenX = (this.getVirtualScrollX() + screenWidth / 2)
                    / screenWidth;
            int destScreenY = (this.getVirtualScrollY() + screenHeight / 2)
                    / screenHeight;
            
            final VelocityTracker velocityTracker = this.mVelocityTracker;
            velocityTracker.computeCurrentVelocity(1000);
            int velocityX = (this.mScrollDirection & ScrollLayout.SCROLL_HORIZONTAL) == 0 ? 0
                    : (int) velocityTracker.getXVelocity();
            int velocityY = (this.mScrollDirection & ScrollLayout.SCROLL_VERTICAL) == 0 ? 0
                    : (int) velocityTracker.getYVelocity();
            Log.d(ScrollLayout.TAG, "velocityX:" + velocityX + ", velocityY:"
                    + velocityY);
            if (velocityX > ScrollLayout.SNAP_VELOCITY
                    && ScrollLayout.mCurScreenX > 0) {
                Log.e(ScrollLayout.TAG, "snap left");
                destScreenX = ScrollLayout.mCurScreenX - 1;
            }
            else if (velocityX < -ScrollLayout.SNAP_VELOCITY
                    && ScrollLayout.mCurScreenX < this.getRightMostScreen()) {
                // Fling enough to move right
                Log.e(ScrollLayout.TAG, "snap right");
                destScreenX = ScrollLayout.mCurScreenX + 1;
            }
            
            if (velocityY > ScrollLayout.SNAP_VELOCITY
                    && ScrollLayout.mCurScreenY > 0) {
                Log.e(ScrollLayout.TAG, "snap up");
                destScreenY = ScrollLayout.mCurScreenY - 1;
            }
            else if (velocityY < -ScrollLayout.SNAP_VELOCITY
                    && ScrollLayout.mCurScreenY < this.getBottomMostScreen()) {
                // Fling enough to move down
                Log.e(ScrollLayout.TAG, "snap down");
                destScreenY = ScrollLayout.mCurScreenY + 1;
            }
            
            this.snapToScreen(destScreenX, destScreenY);
            
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
            }
            
            this.mTouchState = ScrollLayout.TOUCH_STATE_REST;
            break;
        
        case MotionEvent.ACTION_CANCEL:
            this.mTouchState = ScrollLayout.TOUCH_STATE_REST;
            break;
        }
        
        return true;
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(ScrollLayout.TAG, "onInterceptTouchEvent-slop:" + this.mTouchSlop);
        
        final int action = ev.getAction();
        if ((action == MotionEvent.ACTION_MOVE)
                && (this.mTouchState != ScrollLayout.TOUCH_STATE_REST)) {
            return true;
        }
        
        final float x = (this.mScrollDirection & ScrollLayout.SCROLL_HORIZONTAL) == 0 ? 0
                : ev.getX();
        final float y = (this.mScrollDirection & ScrollLayout.SCROLL_VERTICAL) == 0 ? 0
                : ev.getY();
        switch (action) {
        case MotionEvent.ACTION_DOWN:
            this.mLastMotionX = x;
            this.mLastMotionY = y;
            this.mTouchState = this.mScroller.isFinished() ? ScrollLayout.TOUCH_STATE_REST
                    : ScrollLayout.TOUCH_STATE_SCROLLING;
            break;
        
        case MotionEvent.ACTION_MOVE:
            if ((this.mScrollDirection & ScrollLayout.SCROLL_HORIZONTAL) != 0) {
                final int xDiff = (int) Math.abs(this.mLastMotionX - x);
                if (xDiff > this.mTouchSlop) {
                    if (Math.abs(this.mLastMotionY - y)
                            / Math.abs(this.mLastMotionX - x) < 1)
                        this.mTouchState = ScrollLayout.TOUCH_STATE_SCROLLING;
                }
            }
            if (this.mTouchState != ScrollLayout.TOUCH_STATE_SCROLLING
                    && (this.mScrollDirection & ScrollLayout.SCROLL_VERTICAL) != 0) {
                final int yDiff = (int) Math.abs(this.mLastMotionY - y);
                if (yDiff > this.mTouchSlop) {
                    if (Math.abs(this.mLastMotionY - y)
                            / Math.abs(this.mLastMotionX - x) > 1)
                        this.mTouchState = ScrollLayout.TOUCH_STATE_SCROLLING;
                }
            }
            
            break;
        
        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP:
            this.mTouchState = ScrollLayout.TOUCH_STATE_REST;
            break;
        }
        
        return this.mTouchState != ScrollLayout.TOUCH_STATE_REST;
    }
    
    /**
     * scroll screen callback
     * 
     * @param whichScreenX
     * @param whichScreenY
     */
    private void doScrollAction(int whichScreenX, int whichScreenY) {
        if (this.onScrollToScreen != null) {
            this.onScrollToScreen.doAction(whichScreenX, whichScreenY);
        }
    }
    
    /**
     * set callback for OnScrollToScreenListener
     * 
     * @param paramOnScrollToScreen
     */
    public void setOnScrollToScreenListener(
            OnScrollToScreenListener paramOnScrollToScreen) {
        this.onScrollToScreen = paramOnScrollToScreen;
    }
    
    /**
     * OnScrollToScreenListener callback interface
     */
    public abstract interface OnScrollToScreenListener {
        public void doAction(int whichScreenX, int whichScreenY);
    }
    
    /**
     * set default screen position
     * 
     * @param positionX
     * @param positionY
     */
    public void setDefaultScreen(int positionX, int positionY) {
        ScrollLayout.mCurScreenX = positionX;
        ScrollLayout.mCurScreenY = positionY;
    }
    
    private int getVirtualScrollX() {
        if ((this.mScrollDirection & ScrollLayout.SCROLL_HORIZONTAL) != 0) {
            return this.getScrollX();
        }
        else {
            return 0;
        }
    }
    
    private int getVirtualScrollY() {
        if ((this.mScrollDirection & ScrollLayout.SCROLL_VERTICAL) != 0) {
            return this.getScrollY();
        }
        else {
            return 0;
        }
    }
    
    public int getRightMostScreen() {
        int sc = this.getChildCount();
        if (sc > 0)
            sc--;
        return sc;
    }
    
    public int getBottomMostScreen() {
        int sc = this.getChildCount();
        if (sc > 0)
            sc--;
        return sc;
    }
    
    public int getScrollDirection() {
        return this.mScrollDirection;
    }
    
    public void setScrollDirection(int scrollDirection) {
        this.mScrollDirection = scrollDirection;
    }
}
