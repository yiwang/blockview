package com.stpi.BlockView.lib;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;
import android.widget.GridLayout;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class BlockView extends GridLayout {
    String TAG = "ButtonGridView";
    
    private int mNumOfColumn = -1, mNumOfRow = -1;
    private ArrayList<GridItem> mItems;
    
    private int mLayoutWidth = 1000;
    private int mLayoutHeight = 3000;
    
    public BlockView(Context context) {
        super(context);
        this.init(null, 0);
    }
    
    public BlockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs, 0);
    }
    
    public BlockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(attrs, defStyle);
    }
    
    private void init(AttributeSet attrs, int defStyle) {
        
        // Load attributes
        final TypedArray a = this.getContext().obtainStyledAttributes(attrs,
                R.styleable.BlockGridView, defStyle, 0);
        
        try {
            // Retrieve the values from the TypedArray and store into
            // fields of this class.
            
            this.mNumOfColumn = a.getInteger(
                    R.styleable.BlockGridView_numOfColumns, this.mNumOfColumn);
            this.mNumOfRow = a.getInteger(R.styleable.BlockGridView_numOfRows,
                    this.mNumOfRow);
            this.mLayoutWidth = a.getDimensionPixelSize(
                    R.styleable.BlockGridView_layoutWidth, this.mLayoutWidth);
            this.mLayoutHeight = a.getDimensionPixelSize(
                    R.styleable.BlockGridView_layoutHeight, this.mLayoutHeight);
            
        } finally {
            // release the TypedArray so that it can be reused.
            a.recycle();
        }
        
        this.mItems = new ArrayList<GridItem>();
        
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
        params.setGravity(Gravity.CENTER);
        params.setMargins(0, 0, 0, 0);
        this.setLayoutParams(params);
        this.resizeItemViews();
    }
    
    public void addItems(GridItem items[]) {
        for (GridItem item : items) {
            this.addItem(item);
        }
        this.invalidate();
    }
    
    public void addItem(GridItem item) {
        this.mItems.add(item);
        this.invalidate();
    }
    
    public void removeItem(int index) {
        this.mItems.remove(index);
        this.invalidate();
    }
    
    public GridItem getItem(int index) {
        return this.mItems.get(index);
    }
    
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, b, b, b, b);
        this.resizeItemViews();
    }
    
    private void resizeItemViews() {
        int margin = 5;
        int width = this.getLayoutWidth() / this.mNumOfColumn - margin * 2;
        int height = this.getLayoutHeight() / this.mNumOfRow - margin * 2;
        
        for (GridItem item : this.mItems) {
            Button button = new Button(this.getContext());
            
            button.setBackgroundColor(item.color);
            button.setHint(item.title);
            button.setText(item.title);
            button.setTextColor(item.textColor);
            
            if (item.layoutParams.width <= 0) {
                button.setWidth(width);
            }
            if (item.layoutParams.height <= 0) {
                button.setHeight(height);
            }
            button.setLayoutParams(item.layoutParams);
            
            button.setOnClickListener(item.mListener);
            this.addView(button);
        }
    }
    
    public int getNumOfColumn() {
        return this.mNumOfColumn;
    }
    
    public void setNumOfColumn(int columns) {
        this.mNumOfColumn = columns;
        this.invalidate();
    }
    
    public int getNumOfRow() {
        return this.mNumOfRow;
    }
    
    public void setNumOfRow(int rows) {
        this.mNumOfRow = rows;
        this.invalidate();
    }
    
    public int getLayoutWidth() {
        return this.mLayoutWidth;
    }
    
    public void setLayoutWidth(int mLayoutWidth) {
        this.mLayoutWidth = mLayoutWidth;
        this.invalidate();
    }
    
    public int getLayoutHeight() {
        return this.mLayoutHeight;
    }
    
    public void setLayoutHeight(int mLayoutHeight) {
        this.mLayoutHeight = mLayoutHeight;
        this.invalidate();
    }
}
