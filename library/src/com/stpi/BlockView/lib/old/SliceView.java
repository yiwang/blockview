package com.stpi.BlockView.lib.old;

import com.stpi.BlockView.lib.R;
import com.stpi.BlockView.lib.R.styleable;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * SliceView.
 */
public class SliceView extends ViewGroup {
    // private final String TAG = this.getClass().getName();
    
    protected final int TAG_VIEW_PARAMS = 0x12344123;
    
    /** The amount of space used by children in the horizontal gutter. */
    private int mHortizontalGutter = 0;
    
    /** The amount of space used by children in the vertical gutter. */
    private int mVerticalGutter = 0;
    
    private int mRowSpanUnit = 100;
    private int mColumnSpanUnit = 100;
    
    class SliceParams {
        public SliceParams(int row, int column) {
            this.init(row, column, 1, 1);
        }
        
        public SliceParams(int row, int column, int rowSpan, int columnSpan) {
            this.init(row, column, rowSpan, columnSpan);
        }
        
        void init(int row, int column, int rowSpan, int columnSpan) {
            this.row = row;
            this.column = column;
            this.columnSpan = columnSpan;
            this.rowSpan = rowSpan;
        }
        
        int row, column; // the left-top axis
        int columnSpan, rowSpan; // the actual width and height need to
        // multiple mRowSize, for example: actual_width = span * columnSpan;
    }
    
    public SliceView(Context context) {
        super(context);
        this.init(null, 0);
    }
    
    public SliceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs, 0);
    }
    
    public SliceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(attrs, defStyle);
    }
    
    private void init(AttributeSet attrs, int defStyle) {
        
        this.setWillNotDraw(true);
        this.setClipToPadding(true);
        
        // Load attributes
        final TypedArray a = this.getContext().obtainStyledAttributes(attrs,
                R.styleable.SliceView, defStyle, 0);
        
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing
        // with
        // values that should fall on pixel boundaries.
        this.mHortizontalGutter = a.getDimensionPixelSize(
                R.styleable.SliceView_hortizontalGutter,
                this.mHortizontalGutter);
        this.mVerticalGutter = a.getDimensionPixelSize(
                R.styleable.SliceView_verticalGutter, this.mVerticalGutter);
        this.mRowSpanUnit = a.getDimensionPixelSize(
                R.styleable.SliceView_rowSpanUnit, this.mRowSpanUnit);
        this.mColumnSpanUnit = a.getDimensionPixelSize(
                R.styleable.SliceView_columnSpanUnit, this.mColumnSpanUnit);
        
        a.recycle();
    }
    
    public void addView(View view, int row, int column, int rowSpan,
            int columnSpan) {
        view.setTag(this.TAG_VIEW_PARAMS, new SliceParams(row, column, rowSpan,
                columnSpan));
        this.addView(view);
        this.calculateViewSize();
    }
    
    public void setRowSpanUnit(int length) {
        this.mRowSpanUnit = length;
        this.invalidate();
    }
    
    public int getRowSpanUnit() {
        return this.mRowSpanUnit;
    }
    
    public void setColumnSpanUnit(int length) {
        this.mColumnSpanUnit = length;
        this.invalidate();
    }
    
    public float getColumnSpanUnit() {
        return this.mColumnSpanUnit;
    }
    
    public void setHortizontalGutter(int gutter) {
        this.mHortizontalGutter = gutter;
        this.invalidate();
    }
    
    public int getHortizontalGutter() {
        return this.mHortizontalGutter;
    }
    
    public void setVerticalGutter(int gutter) {
        this.mVerticalGutter = gutter;
        this.invalidate();
    }
    
    public int getVerticalGutter() {
        return this.mVerticalGutter;
    }
    
    /**
     * Ask all children to measure themselves and compute the measurement of
     * this layout based on the children.
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    
    /**
     * Position all children within this layout.
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
    }
    
    private void calculateViewSize() {
        final int count = this.getChildCount();
        int leftPos = this.getPaddingLeft();
        final int parentTop = this.getPaddingTop();
        int minOfWidth = 0;
        int minOfHeight = 0;
        for (int i = 0; i < count; i++) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                SliceParams sp = (SliceParams) child
                        .getTag(this.TAG_VIEW_PARAMS);
                if (sp != null) {
                    child.setLeft(leftPos + sp.column * this.mColumnSpanUnit);
                    
                    int rightEdge = leftPos + (sp.column + sp.columnSpan)
                            * this.mColumnSpanUnit;
                    child.setRight(rightEdge - this.mHortizontalGutter / 2);
                    if (minOfWidth < rightEdge) {
                        minOfWidth = rightEdge;
                    }
                    
                    child.setTop(parentTop + sp.row * this.mRowSpanUnit);
                    
                    int bottomEdge = parentTop + (sp.row + sp.rowSpan)
                            * this.mRowSpanUnit;
                    child.setBottom(bottomEdge - this.mVerticalGutter / 2);
                    if (minOfHeight < bottomEdge) {
                        minOfHeight = bottomEdge;
                    }
                }
            }
        }
        
        minOfWidth += this.getPaddingRight();
        minOfHeight += this.getPaddingBottom();
        this.setMinimumWidth(minOfWidth);
        this.setMinimumHeight(minOfHeight);
    }
    
}
