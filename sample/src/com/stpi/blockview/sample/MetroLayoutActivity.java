package com.stpi.blockview.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

import com.stpi.BlockView.lib.MetroLayout;
import com.stpi.BlockView.lib.ScrollLayout;

public class MetroLayoutActivity extends Activity {
    
    MetroLayout ml;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_metro_layout);
        this.ml = (MetroLayout) this.findViewById(R.id.MetroLayout);
        
        this.ml.setColumnCount(3);
        this.ml.setRowCount(5);
        this.ml.setScrollDirection(ScrollLayout.SCROLL_HORIZONTAL
                | ScrollLayout.SCROLL_VERTICAL);
        
        Button bt = new Button(this);
        bt.setText("hello");
        MetroLayout.LayoutParams lp = new MetroLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp.x = 1;
        lp.y = 1;
        lp.xSpan = 1;
        lp.ySpan = 1;
        bt.setLayoutParams(lp);
        this.ml.addView(bt);
        
        Button bt2 = new Button(this);
        bt2.setText("hello2");
        MetroLayout.LayoutParams lp2 = new MetroLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp2.x = 2;
        lp2.y = 1;
        lp2.xSpan = 2;
        lp2.ySpan = 2;
        bt2.setLayoutParams(lp2);
        this.ml.addView(bt2);
        
        Button bt3 = new Button(this);
        bt3.setText("hello3");
        MetroLayout.LayoutParams lp3 = new MetroLayout.LayoutParams(200, 300);
        lp3.x = 0;
        lp3.y = 0;
        lp3.xSpan = 1;
        lp3.ySpan = 2;
        lp3.gravity = Gravity.TOP | Gravity.LEFT;
        bt3.setLayoutParams(lp3);
        this.ml.addView(bt3);
        
        View bt4 = new View(this);
        bt4.setBackgroundColor(0x40FF0040);
        MetroLayout.LayoutParams lp4 = new MetroLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp4.x = 0;
        lp4.y = 0;
        lp4.xSpan = 1;
        lp4.ySpan = 2;
        bt4.setLayoutParams(lp4);
        this.ml.addView(bt4);
        
        Button bt5 = new Button(this);
        bt5.setText("hello5");
        MetroLayout.LayoutParams lp5 = new MetroLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp5.x = 3;
        lp5.y = 5;
        lp5.xSpan = 1;
        lp5.ySpan = 2;
        lp5.gravity = Gravity.TOP | Gravity.LEFT;
        bt5.setLayoutParams(lp5);
        this.ml.addView(bt5);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.getMenuInflater().inflate(R.menu.metro_layout, menu);
        return true;
    }
    
}
