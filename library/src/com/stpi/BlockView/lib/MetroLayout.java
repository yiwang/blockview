/**
 * 
 */
package com.stpi.BlockView.lib;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author yiwang
 * 
 */
public class MetroLayout extends ScrollLayout {
    private static final String TAG = "MetroLayout";
    
    protected final int TAG_VIEW_PARAMS = 0x12344123;
    
    /** The amount of space used by children in the horizontal gutter. */
    private int mXGutter = 0;
    
    /** The amount of space used by children in the vertical gutter. */
    private int mYGutter = 0;
    
    private int mMaxColumn = 0;
    private int mMaxRow = 0;
    
    /**
     * The left padding in pixels, that is the distance in pixels between the
     * left edge of this view and the left edge of its content.
     */
    protected int mPaddingLeft = 0;
    /**
     * The right padding in pixels, that is the distance in pixels between the
     * right edge of this view and the right edge of its content.
     */
    protected int mPaddingRight = 0;
    /**
     * The top padding in pixels, that is the distance in pixels between the top
     * edge of this view and the top edge of its content.
     */
    protected int mPaddingTop;
    /**
     * The bottom padding in pixels, that is the distance in pixels between the
     * bottom edge of this view and the bottom edge of its content.
     */
    protected int mPaddingBottom;
    
    private int mColumnCount = 1;
    private int mRowCount = 1;
    
    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        /*
         * Special value for the height or width requested by a View. map
         * MATCH_PARENT to FILL_GRID.
         */
        public static final int FILL_GRID = -1;
        
        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            this.init(0, 0, 1, 1);
        }
        
        public LayoutParams(int width, int height) {
            super(width, height);
            this.init(0, 0, 1, 1);
        }
        
        public LayoutParams(int width, int height, int x, int y, int xSpan,
                int ySpan) {
            super(width, height);
            this.init(x, y, xSpan, ySpan);
        }
        
        void init(int x, int y, int xSpan, int ySpan) {
            this.x = x;
            this.y = y;
            this.xSpan = xSpan;
            this.ySpan = ySpan;
        }
        
        public int x, y; // the left-top axis
        public int xSpan, ySpan; // the actual width and height need to
        // multiple mRowSize, for example: actual_width = span * columnSpan;
        public int gravity = Gravity.CENTER;
    }
    
    public MetroLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }
    
    void init(Context context, AttributeSet attrs) {
        
    }
    
    public void addView(View child, int x, int y, int xSpan, int ySpan) {
        MetroLayout.LayoutParams lp = new MetroLayout.LayoutParams(
                LayoutParams.FILL_GRID, LayoutParams.FILL_GRID);
        child.getLayoutParams();
        lp.x = x;
        lp.y = y;
        lp.xSpan = xSpan;
        lp.ySpan = ySpan;
        this.addView(child, lp);
    }
    
    public void setHortizontalGutter(int gutter) {
        this.mXGutter = gutter;
        this.invalidate();
    }
    
    public int getHortizontalGutter() {
        return this.mXGutter;
    }
    
    public void setVerticalGutter(int gutter) {
        this.mYGutter = gutter;
        this.invalidate();
    }
    
    public int getVerticalGutter() {
        return this.mYGutter;
    }
    
    /*
     * (non-Javadoc)
     * @see android.view.ViewGroup#onLayout(boolean, int, int, int, int)
     */
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        Log.d(MetroLayout.TAG, "onLayout: (" + l + "," + t + "," + r + "," + b
                + ")");
        this.layoutChildViews(l, t, r, b);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int width = MeasureSpec.getSize(widthMeasureSpec);
        final int height = MeasureSpec.getSize(heightMeasureSpec);
        final int paddingLeft = this.mPaddingLeft;
        final int paddingTop = this.mPaddingTop;
        final int xSpan = (width - paddingLeft - this.mPaddingRight)
                / this.mColumnCount;
        final int ySpan = (height - paddingTop - this.mPaddingBottom)
                / this.mRowCount;
        
        Log.d(MetroLayout.TAG, "onMeasure: (" + widthMeasureSpec + ","
                + heightMeasureSpec + "," + width + "," + height + ")");
        
        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        if (widthMode != MeasureSpec.EXACTLY) {
            throw new IllegalStateException(
                    "ScrollLayout only canmCurScreen run at EXACTLY mode!");
        }
        
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (heightMode != MeasureSpec.EXACTLY) {
            throw new IllegalStateException(
                    "ScrollLayout only can run at EXACTLY mode!");
        }
        
        // The children are given the same width and height as the scrollLayout
        final int count = this.getVirtualChildCount();
        for (int i = 0; i < count; i++) {
            final View child = this.getVirtualChildAt(i);
            LayoutParams lp = (LayoutParams) child.getLayoutParams();
            if (lp.width == LayoutParams.FILL_GRID) {
                lp.width = lp.xSpan * xSpan;
            }
            if (lp.height == LayoutParams.FILL_GRID) {
                lp.height = lp.ySpan * ySpan;
            }
            final int childWidthMeasureSpec = ViewGroup.getChildMeasureSpec(
                    widthMeasureSpec, this.mPaddingLeft + this.mPaddingRight
                            + lp.leftMargin + lp.rightMargin, lp.width);
            final int childHeightMeasureSpec = ViewGroup.getChildMeasureSpec(
                    widthMeasureSpec, this.mPaddingTop + this.mPaddingBottom
                            + lp.topMargin + lp.bottomMargin, lp.height);
            child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
        }
    }
    
    public void setItemMargins() {
        
    }
    
    static int sBKColor = 0x400000FF;
    
    /**
     * @return the columnCount
     */
    public int getColumnCount() {
        return this.mColumnCount;
    }
    
    /**
     * @param columnCount
     *            the columnCount to set
     */
    public void setColumnCount(int count) {
        this.mColumnCount = count;
        this.invalidate();
    }
    
    public void setRowCount(int count) {
        this.mRowCount = count;
        this.invalidate();
    }
    
    public int getRowCount() {
        return this.mRowCount;
    }
    
    /**
     * Position the childrens
     * 
     * @see #onLayout(boolean, int, int, int, int)
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    void layoutChildViews(int left, int top, int right, int bottom) {
        final int width = right - left;
        final int height = bottom - top;
        final int paddingLeft = this.mPaddingLeft;
        final int paddingTop = this.mPaddingTop;
        final int xSpan = (width - paddingLeft - this.mPaddingRight)
                / this.mColumnCount;
        final int ySpan = (height - paddingTop - this.mPaddingBottom)
                / this.mRowCount;
        
        int childTop = 0;
        int childLeft = 0;
        final int count = this.getVirtualChildCount();
        for (int i = 0; i < count; i++) {
            final View child = this.getVirtualChildAt(i);
            if (child == null) {
                
            }
            else if (child.getVisibility() != View.GONE) {
                final int childWidth = child.getMeasuredWidth();
                final int childHeight = child.getMeasuredHeight();
                final MetroLayout.LayoutParams lp = (MetroLayout.LayoutParams) child
                        .getLayoutParams();
                final int hSpace = lp.xSpan * xSpan;
                final int vSpace = lp.ySpan * ySpan;
                final int rightMostX = lp.x + lp.xSpan;
                final int bottomMostY = lp.y + lp.ySpan;
                
                if (rightMostX > this.mMaxColumn) {
                    this.mMaxColumn = rightMostX;
                }
                if (bottomMostY > this.mMaxColumn) {
                    this.mMaxRow = bottomMostY;
                }
                
                int gravity = lp.gravity;
                switch (gravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
                case Gravity.CENTER_HORIZONTAL:
                    childLeft = paddingLeft + lp.x * xSpan
                            + ((hSpace - childWidth) / 2) + lp.leftMargin
                            - lp.rightMargin;
                    break;
                
                case Gravity.RIGHT:
                    childLeft = paddingLeft + rightMostX * xSpan - childWidth
                            - lp.rightMargin;
                    break;
                
                case Gravity.LEFT:
                default:
                    childLeft = paddingLeft + lp.x * xSpan + lp.leftMargin;
                    break;
                }
                
                switch (gravity & Gravity.VERTICAL_GRAVITY_MASK) {
                case Gravity.CENTER_VERTICAL:
                    childTop = paddingTop + lp.y * ySpan
                            + ((vSpace - childHeight) / 2) + lp.topMargin
                            - lp.bottomMargin;
                    break;
                
                case Gravity.BOTTOM:
                    childTop = paddingTop + bottomMostY * ySpan - childHeight
                            - lp.bottomMargin;
                    break;
                
                case Gravity.TOP:
                default:
                    childTop = paddingTop + lp.y * ySpan + lp.topMargin;
                    break;
                }
                
                this.setChildFrame(child, childLeft, childTop, childWidth,
                        childHeight);
                
                i += this.getChildrenSkipCount(child, i);
            }
        }
    }
    
    /**
     * <p>
     * Returns the view at the specified index. This method can be overriden to
     * take into account virtual children. Refer to
     * {@link android.widget.TableLayout} and {@link android.widget.TableRow}
     * for an example.
     * </p>
     * 
     * @param index
     *            the child's index
     * @return the child at the specified index
     */
    View getVirtualChildAt(int index) {
        return this.getChildAt(index);
    }
    
    /**
     * <p>
     * Returns the virtual number of children. This number might be different
     * than the actual number of children if the layout can hold virtual
     * children. Refer to {@link android.widget.TableLayout} and
     * {@link android.widget.TableRow} for an example.
     * </p>
     * 
     * @return the virtual number of children
     */
    int getVirtualChildCount() {
        return this.getChildCount();
    }
    
    /**
     * <p>
     * Returns the number of children to skip after measuring/laying out the
     * specified child.
     * </p>
     * 
     * @param child
     *            the child after which we want to skip children
     * @param index
     *            the index of the child after which we want to skip children
     * @return the number of children to skip, 0 by default
     */
    int getChildrenSkipCount(View child, int index) {
        return 0;
    }
    
    /**
     * <p>
     * Returns the size (width or height) that should be occupied by a null
     * child.
     * </p>
     * 
     * @param childIndex
     *            the index of the null child
     * @return the width or height of the child depending on the orientation
     */
    int measureNullChild(int childIndex) {
        return 0;
    }
    
    private void setChildFrame(View child, int left, int top, int width,
            int height) {
        child.layout(left, top, left + width, top + height);
    }
    
    @Override
    public int getRightMostScreen() {
        return (this.mMaxColumn - 1) / this.mColumnCount;
    }
    
    @Override
    public int getBottomMostScreen() {
        return (this.mMaxRow - 1) / this.mRowCount;
    }
}
